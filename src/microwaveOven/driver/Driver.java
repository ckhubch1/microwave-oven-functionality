package microwaveOven.driver;

import microwaveOven.util.FileProcessor;
import microwaveOven.service.MicrowaveContext;

public class Driver
{
	public static void main(String[] args)
	{
		if (args.length != 0)
		{
			System.err.println("Error: Command line args should not be passed");
			System.exit(0);
		}

		int count;
		String input;
		FileProcessor FPObj = new FileProcessor();
		MicrowaveContext mcObj = new MicrowaveContext();
		FPObj.OpenFile();
		count = FPObj.CountLines();
		for (int i = 0; i < count; i++)
		{
			input = FPObj.readLine();
			// System.out.println(input+"\n");
			mcObj.performAction(input);
		}
		FPObj.CloseFile();
	}

}
