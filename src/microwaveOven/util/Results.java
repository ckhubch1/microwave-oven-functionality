package microwaveOven.util;

import java.io.FileWriter;
import java.io.IOException;

import microwaveOven.util.FileDisplayInterface;
import microwaveOven.util.StdOutDisplayInterface;

public class Results implements FileDisplayInterface, StdOutDisplayInterface
{
	public static int val = 1;
	FileWriter write = null;

	public void writeToScreen(String s)
	{
		System.out.println(s);
	}

	public void writeToFile(String s)
	{
		try
		{

			if (val == 1)
			{
				FileWriter clear = new FileWriter("output.txt");
				clear.write("");
				clear.close();
				val = 0;
			}

			write = new FileWriter("output.txt", true);
			write.write(s + "\n");
			write.close();
		}

		catch (IOException e)
		{
			System.err.println("Error while writing to output.txt");
		}

	}
}
