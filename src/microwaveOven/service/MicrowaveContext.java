package microwaveOven.service;

public class MicrowaveContext
{
	int numberSet;
	public static int currentState = 1;
	public static int cookingTime;
	TimeDisplayStateImpl TD = new TimeDisplayStateImpl();
	NumberStateImpl NS = new NumberStateImpl();
	CookingStateImpl CS = new CookingStateImpl();
	NotCookingStateImpl NC = new NotCookingStateImpl();
	SetClockStateImpl SC = new SetClockStateImpl();

	public void performAction(String input)
	{
		if (input.matches("[0-9]+"))
		{
			cookingTime = Integer.parseInt(input);
			if (1 + (int) Math.log10(cookingTime) <= 4)
			{
				switch (currentState)
				{
				case 1:
					currentState = TD.action(1);
					break;
				case 2:
					currentState = NS.action(1);
					break;
				case 3:
					currentState = CS.action(1);
					break;
				case 4:
					currentState = NC.action(1);
					break;
				case 5:
					currentState = SC.action(1);
					break;
				default:
					System.out.println("No such input exists!");
					System.exit(0);
					break;
				}
			} else
				System.out.println("Beep, Entered Time has too many digits\n");
		} else
		{
			switch (input)
			{
			case "Start":
				startAction(cookingTime);
				break;
			case "Stop":
				stopAction();
				break;
			case "SetClock":
				setClockAction();
				break;
			default:
				System.out.println("Wrong input\n");
				System.exit(0);
				break;

			}
		}

	}

	public void startAction(int time)
	{
		switch (currentState)
		{
		case 1:
			currentState = TD.action(2);
			break;

		case 2:
			currentState = NS.action(2);
			break;

		case 3:
			currentState = CS.action(2);
			break;

		case 4:
			currentState = NC.action(2);
			break;
		case 5:
			currentState = SC.action(2);
			break;
		default:
			System.out.println("Wrong Input to Start Action!");
			System.exit(0);
			break;
		}
	}

	public void stopAction()
	{
		switch (currentState)
		{
		case 1:
			currentState = TD.action(3);
			break;

		case 2:
			currentState = NS.action(3);
			break;

		case 3:
			currentState = CS.action(3);
			break;

		case 4:
			currentState = NC.action(3);
			break;
		case 5:
			currentState = SC.action(3);
			break;
		default:
			System.out.println("Wrong Input to Stop Action!");
			System.exit(0);
			break;
		}
	}

	public void setClockAction()
	{
		switch (currentState)
		{
		case 1:
			currentState = TD.action(4);
			break;

		case 2:
			currentState = NS.action(4);
			break;

		case 3:
			currentState = CS.action(4);
			break;

		case 4:
			currentState = NC.action(4);
			break;
		case 5:
			currentState = SC.action(4);
			break;
		default:
			System.out.println("Wrong Input in Start State!");
			System.exit(0);
			break;
		}
	}

}
