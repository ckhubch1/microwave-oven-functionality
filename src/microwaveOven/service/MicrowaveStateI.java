package microwaveOven.service;

public interface MicrowaveStateI
{
	public int inputNumbers();

	public int setStartAction(int digits);

	public int cancelStopAction();

	public int setClockAction();

}
