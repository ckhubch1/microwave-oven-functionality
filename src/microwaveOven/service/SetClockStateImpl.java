package microwaveOven.service;

import static microwaveOven.service.MicrowaveContext.cookingTime;
import static microwaveOven.service.TimeDisplayStateImpl.clockTime;
import microwaveOven.util.Results;

public class SetClockStateImpl implements MicrowaveStateI
{
	Results results = new Results();

	public int action(int MethodAction)
	{
		int changeState = 0;
		switch (MethodAction)
		{
		case 1:
			changeState = inputNumbers();
			break;
		case 2:
			changeState = setStartAction(cookingTime);
			break;
		case 3:
			changeState = cancelStopAction();
			break;
		case 4:
			changeState = setClockAction();
			break;
		default:
			System.out.println("Error, no such method!");
			System.exit(0);
			break;
		}

		return changeState;
	}

	public int inputNumbers()
	{
		// Do nothing and return the same state
		results.writeToScreen("Clock time set: " + cookingTime + "\n");
		results.writeToFile("Clock time set: " + cookingTime + "\n");
		clockTime = cookingTime;
		return 5;
	}

	public int setStartAction(int time)
	{
		if (clockTime != -1)
		{
			TimeDisplayStateImpl TD = new TimeDisplayStateImpl();
			return TD.setStartAction(clockTime);
		} else
		{
			results.writeToScreen("Start Button Inactive\nClock Time not entered\n");
			results.writeToFile("Start Button Inactive\nClock Time not entered\n");
			return 5;
		}

	}

	public int cancelStopAction()
	{
		results.writeToScreen("Cancel Button Pressed\nButton Inactive\n");
		results.writeToFile("Cancel Button Pressed\nButton Inactive\n");
		return 5;

	}

	public int setClockAction()
	{
		results.writeToScreen("Set Clock Button Pressed\nButton Inactive\n");
		results.writeToFile("Set Clock Button Pressed\nButton Inactive\n");
		return 5;
	}
}
