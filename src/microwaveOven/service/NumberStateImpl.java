package microwaveOven.service;

import static microwaveOven.service.MicrowaveContext.cookingTime;
import microwaveOven.util.Results;

public class NumberStateImpl implements MicrowaveStateI
{
	Results results = new Results();

	public int action(int MethodAction)
	{
		int changeState = 0;
		switch (MethodAction)
		{
		case 1:
			changeState = inputNumbers();
			break;
		case 2:
			changeState = setStartAction(cookingTime);
			break;
		case 3:
			changeState = cancelStopAction();
			break;
		case 4:
			changeState = setClockAction();
			break;
		default:
			System.out.println("Error, no such method!");
			System.exit(0);
			break;
		}

		return changeState;
	}

	public int inputNumbers()
	{
		// Do nothing and return the same state
		results.writeToScreen("Numbers Entered\nNo Action Performed\n");
		results.writeToFile("Numbers Entered\nNo Action Performed\n");
		return 2;
	}

	public int setStartAction(int time)
	{
		results.writeToScreen("Start Button Pressed\nCooking started\n");
		results.writeToFile("Start Button Pressed\nCooking started\n");
		return 3;

	}

	public int cancelStopAction()
	{
		TimeDisplayStateImpl TD = new TimeDisplayStateImpl();
		return TD.cancelStopAction();

	}

	public int setClockAction()
	{
		results.writeToScreen("SetClock Button Pressed\nButton Inactive\n");
		results.writeToFile("SetClock Button Pressed\nButton Inactive\n");
		return 2;
	}
}
