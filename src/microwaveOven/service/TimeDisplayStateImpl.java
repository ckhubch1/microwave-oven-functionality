package microwaveOven.service;

import microwaveOven.util.Results;
import java.util.Calendar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import static microwaveOven.service.MicrowaveContext.cookingTime;

public class TimeDisplayStateImpl implements MicrowaveStateI
{
	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	Calendar cal = Calendar.getInstance();
	Results results = new Results();
	public static int clockTime = -1;

	public int action(int MethodAction)
	{
		int changeState = 0;
		switch (MethodAction)
		{
		case 1:
			changeState = inputNumbers();
			break;
		case 2:
			changeState = setStartAction(cookingTime);
			break;
		case 3:
			changeState = cancelStopAction();
			break;
		case 4:
			changeState = setClockAction();
			break;
		default:
			System.out.println("Error, no such method!");
			System.exit(0);
			break;
		}

		return changeState;
	}

	public int inputNumbers()
	{
		results.writeToScreen("Numbers Entered\nCookingTime set to: " + cookingTime + "\n");
		results.writeToFile("Numbers Entered\nCookingTime set to: " + cookingTime + "\n");
		return 2;
	}

	public int setStartAction(int time)
	{
		if (clockTime == -1)
		{
			results.writeToScreen("Start Button pressed\nDisplay: " + dateFormat.format(cal.getTime()) + "\n");
			results.writeToFile("Start Button pressed\nDisplay: " + dateFormat.format(cal.getTime()) + "\n");
		} else
		{
			results.writeToScreen("Start Button pressed\nDisplay: " + clockTime + "\n");
			results.writeToFile("Start Button pressed\nDisplay: " + clockTime + "\n");

		}
		return 1;
	}

	public int cancelStopAction()
	{
		if (clockTime == -1)
		{
			results.writeToScreen(
					"Stop-Cancel Button pressed-> Cleared \nDisplay: " + dateFormat.format(cal.getTime()) + "\n");
			results.writeToFile(
					"Stop-Cancel Button pressed-> Cleared \nDisplay: " + dateFormat.format(cal.getTime()) + "\n");
		} else
		{
			results.writeToScreen("Stop-Cancel Button pressed-> Cleared \nDisplay: " + clockTime + "\n");
			results.writeToFile("Stop-Cancel Button pressed-> Cleared \nDisplay: " + clockTime + "\n");

		}
		return 1;
	}

	public int setClockAction()
	{
		results.writeToScreen("SetClock Button Pressed\nEnter Clock time then proceed with Set/Start Button\n");
		results.writeToFile("SetClock Button Pressed\nEnter Clock time then proceed with Set/Start Button\n");
		return 5;
	}
}
