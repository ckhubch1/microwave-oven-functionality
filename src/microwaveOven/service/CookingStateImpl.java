package microwaveOven.service;

import static microwaveOven.service.MicrowaveContext.cookingTime;
import microwaveOven.util.Results;

public class CookingStateImpl implements MicrowaveStateI
{
	Results results = new Results();

	public int action(int MethodAction)
	{
		int changeState = 0;
		switch (MethodAction)
		{
		case 1:
			changeState = inputNumbers();
			break;
		case 2:
			changeState = setStartAction(cookingTime);
			break;
		case 3:
			changeState = cancelStopAction();
			break;
		case 4:
			changeState = setClockAction();
			break;
		default:
			System.out.println("Error, no such method!");
			System.exit(0);
			break;
		}

		return changeState;
	}

	public int inputNumbers()
	{
		// Do nothing and return the same state
		results.writeToScreen("Enter number Button Inactive\n");
		results.writeToFile("Enter number Button Inactive\n");
		return 3;
	}

	public int setStartAction(int time)
	{
		// Do nothing and return the same state
		results.writeToScreen("Start Button Inactive\n");
		results.writeToFile("Start Button Inactive\n");
		return 3;

	}

	public int cancelStopAction()
	{
		results.writeToScreen("Stop Button Pressed\nCooking Stopped, Time remaining: " + cookingTime / 2 + "\n");
		results.writeToFile("Stop Button Pressed\nCooking Stopped, Time remaining: " + cookingTime / 2 + "\n");
		return 4;
	}

	public int setClockAction()
	{
		results.writeToScreen("Set Clock Button Inactive\n");
		results.writeToFile("Set Clock Button Inactive\n");
		return 3;
	}
}
